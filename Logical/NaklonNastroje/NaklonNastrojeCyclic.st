(********************************************************************
 * COPYRIGHT -- HP
 ********************************************************************
 * PROGRAM: ZdvihNastroje
 * File: ZdvihNastrojeCyclic.st
 * Author: Michal Zvol�nek
 * Created: 9.6.2015
 ********************************************************************
 * Implementation OF PROGRAM NaklonNastroje
 ********************************************************************)


PROGRAM _CYCLIC
(* TODO : Add your code here *)
	Copy1 := 0;	
	
	(* Prepocet poloh pro zastaveni*)
		
	PozadovanaDolu2 := PozadovanaNaklon - 15; //+50
	PozadovanaNahoru2 := PozadovanaNaklon + 10; //-15
	PozadovanaDolu2Pomalu := PozadovanaDolu2 + 2500;
	PozadovanaNahoru2Pomalu := PozadovanaNahoru2 - 2500;

	
	(* Automaticke najeti Dolu*)
			
	IF BezpecnostOK = 1 AND StrojZAP = 1 AND StartNajetiN = 1 AND StartNajetiV = 0 AND MoveBUTTON = 1 AND Poloha2 > PozadovanaDolu2 THEN
		Automaticky2Dolu := 1;
	ELSIF Automaticky2Dolu = 1 AND Poloha2 <= PozadovanaDolu2 THEN	
		Automaticky2Dolu := 0;
		StartNajetiN := 0;
	ELSIF BezpecnostOK = 0 OR StrojZAP = 0 OR Sipka2Dolu = 1 OR Sipka2Nahoru = 1 OR MoveBUTTON = 0 OR KoncakNaklonDolu = 1 THEN
		Automaticky2Dolu := 0;
	END_IF
		
	 (* Automaticke najeti Nahoru*)
		
	IF BezpecnostOK = 1 AND StrojZAP = 1 AND StartNajetiN = 1 AND StartNajetiV = 0 AND MoveBUTTON = 1 AND Poloha2 < PozadovanaNahoru2 THEN
		Automaticky2Nahoru := 1;
	ELSIF Automaticky2Nahoru = 1 AND Poloha2 >= PozadovanaNahoru2 THEN	
		Automaticky2Nahoru := 0;
		StartNajetiN := 0;
	ELSIF BezpecnostOK = 0 OR StrojZAP = 0 OR Sipka2Dolu = 1 OR Sipka2Nahoru = 1 OR MoveBUTTON = 0 OR KoncakNaklonNahoru = 1 THEN
		Automaticky2Nahoru := 0;
	END_IF
	
	IF BezpecnostOK = 1 AND StrojZAP = 1 THEN
		NaklonVisuStatus := 2;
		IF (Automaticky2Dolu = 1 OR Sipka2Dolu = 1) AND Automaticky2Nahoru = 0 AND Sipka2Nahoru = 0 AND KoncakNaklonDolu = 0 AND GFnaklon = 1 AND GFzdvih = 0 THEN 
			NaklonDolu := 1;
			NaklonVisuStatus := 1;
			NaklonNahoru := 0;
		ELSE
			NaklonDolu := 0;
		END_IF
		
		IF (Automaticky2Nahoru = 1 OR Sipka2Nahoru = 1) AND Automaticky2Dolu = 0 AND Sipka2Dolu = 0 AND KoncakNaklonNahoru = 0 AND GFnaklon = 1 AND GFzdvih = 0 THEN
			NaklonNahoru := 1;
			NaklonVisuStatus := 1;
			NaklonDolu := 0;
		ELSE
			NaklonNahoru := 0;
		END_IF
	END_IF	
				
	IF (Automaticky2Dolu = 1 AND Poloha2 >= PozadovanaDolu2Pomalu) OR (Automaticky2Nahoru = 1 AND Poloha2 <= PozadovanaNahoru2Pomalu) THEN
		Zdvih2Rychle := 1;
	ELSE
		Zdvih2Rychle := 0;
	END_IF
	
	IF Sipka2Dolu = 1 OR Sipka2Nahoru = 1 THEN
		VretenoDolu := 0;
		VretenoNahoru := 0;
	END_IF
		
	IF ((Automaticky2Nahoru = 1 OR Automaticky2Dolu = 1) OR (Sipka2Dolu = 1 OR Sipka2Nahoru = 1)) AND (VretenoDolu = 0 AND VretenoNahoru = 0) AND GFzdvih = 0 THEN	
		GFnaklon := 1;
	ELSE
		GFnaklon := 0;
	END_IF
	
	IF BezpecnostOK = 0 THEN
		NaklonVisuStatus := 0;
		
	END_IF	

	(* pu�t�n� ot��en� v�ete p�i polohov�n�*)
	
	IF 	(NaklonNahoru = 1 OR NaklonDolu = 1) AND VretenoOn = 0 THEN
		OtackyVretena30 := 1;
	ELSE
		OtackyVretena30 := 0;
	END_IF	
	
END_PROGRAM
