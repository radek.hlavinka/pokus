PROGRAM _INIT
(* init program *)

	InitHodnotaZdvihVreteno := PolohaVretenaPermanent - Poloha1;
	AsIOAccWrite_01_Restart.enable := 1;
	AsIOAccWrite_01_Restart.pDeviceName := ADR('IF1.ST1.IF1.ST7');          (*adresa, ktera je videt v I/O configuration na modulu, p.s. string zruzovi *)
	AsIOAccWrite_01_Restart.pChannelName := ADR('CfO_PresetABR02_1_32Bit');
	AsIOAccWrite_01_Restart.value :=  InitHodnotaZdvihVreteno;
	AsIOAccWrite_01_Restart();
	NabeznaPolohuVretena := 1;

	InitHodnotaNaklonVreteno := PolohaNaklonPermanent - Poloha2;
	AsIOAccWrite_02_Restart.enable := 1;
	AsIOAccWrite_02_Restart.pDeviceName := ADR('IF1.ST1.IF1.ST7');          (*adresa, ktera je videt v I/O configuration na modulu, p.s. string zruzovi *)
	AsIOAccWrite_02_Restart.pChannelName := ADR('CfO_PresetABR01_1_32Bit');
	AsIOAccWrite_02_Restart.value :=  InitHodnotaNaklonVreteno;
	AsIOAccWrite_02_Restart();
	NabeznaNaklon := 1;
	
END_PROGRAM

