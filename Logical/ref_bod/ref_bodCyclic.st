PROGRAM _CYCLIC
(* cyclic program *)
	
	
	(*zadavani refencniho bodu zdvih v�etena*)
	test_001 := test_001;
	
	IF Modul1OK = 0 THEN
		NabeznaPolohuVretena := 0;
	ELSE
		IF NabeznaPolohuVretena = 1 THEN
			PolohaVretenaPermanent := Poloha1;
		ELSE
			AsIOAccWrite_01_Restart.enable := 1;
			AsIOAccWrite_01_Restart.pDeviceName := ADR('IF1.ST1.IF1.ST7');          (*adresa, ktera je videt v I/O configuration na modulu, p.s. string zruzovi *)
			AsIOAccWrite_01_Restart.pChannelName := ADR('CfO_PresetABR02_1_32Bit');
			AsIOAccWrite_01_Restart.value :=  PolohaVretenaPermanent;
			AsIOAccWrite_01_Restart();
			NabeznaPolohuVretena := 1;
		
		END_IF
	END_IF


	Reference1 := Reference1;

	
	IF Zapsat1 = 1 AND Modul1OK = 1 THEN
		AsIOAccWrite_01.enable := 1;
		AsIOAccWrite_01.pDeviceName := ADR('IF1.ST1.IF1.ST7');          (*adresa, ktera je videt v I/O configuration na modulu*)
		AsIOAccWrite_01.pChannelName := ADR('CfO_PresetABR02_1_32Bit');
		AsIOAccWrite_01.value :=  PozadovanaHodnota1;
		
		
//		Cykl1 := Cykl1 +1;
		IF AsIOAccWrite_01.status = ERR_OK THEN
			Zapsat1 := 0;
			AsIOAccWrite_01.enable := FALSE;
//			Cykl1 := 0;
		END_IF
	ELSE
		AsIOAccWrite_01.enable := FALSE;
	END_IF
	
	AsIOAccWrite_01();


	IF Uloz1 = 1 THEN
		IntCounter1 := Poloha1 - AsIOAccWrite_01.value;
		PozadovanaHodnota1 := PrepoctenaKalibrace1 - IntCounter1;
		Uloz1 := 0;
		Zapsat1 := 1;
	END_IF


	
	
	
	
	(*zadavani refencniho bodu pro n�klon vretena*)
	
	IF Modul1OK = 0 THEN
		NabeznaNaklon := 0;
	ELSE
		IF NabeznaNaklon = 1 THEN
			PolohaNaklonPermanent := Poloha2;
		ELSE
			AsIOAccWrite_02_Restart.enable := 1;
			AsIOAccWrite_02_Restart.pDeviceName := ADR('IF1.ST1.IF1.ST7');          (*adresa, ktera je videt v I/O configuration na modulu, p.s. string zruzovi *)
			AsIOAccWrite_02_Restart.pChannelName := ADR('CfO_PresetABR01_1_32Bit');
			AsIOAccWrite_02_Restart.value :=  PolohaNaklonPermanent;
			AsIOAccWrite_02_Restart();
			NabeznaNaklon := 1;
		END_IF
	END_IF


	Reference2 := Reference2;

	
	IF Zapsat2 = 1 AND Modul1OK = 1 THEN
		AsIOAccWrite_02.enable := 1;
		AsIOAccWrite_02.pDeviceName := ADR('IF1.ST1.IF1.ST7');          (*adresa, ktera je videt v I/O configuration na modulu*)
		AsIOAccWrite_02.pChannelName := ADR('CfO_PresetABR01_1_32Bit');
		AsIOAccWrite_02.value :=  PozadovanaHodnota2;
		
		
//		Cykl2 := Cykl2 +1;
		IF AsIOAccWrite_02.status = ERR_OK THEN
			Zapsat2 := 0;
			AsIOAccWrite_02.enable := FALSE;
//			Cykl2 := 0;
		END_IF
	ELSE
		AsIOAccWrite_02.enable := FALSE;
	END_IF

	AsIOAccWrite_02();


	IF Uloz2 = 1 THEN
		IntCounter2 := Poloha2 - AsIOAccWrite_02.value;
		PozadovanaHodnota2 := PrepoctenaKalibrace2 - IntCounter2;
		Uloz2 := 0;
		Zapsat2 := 1;
	END_IF
	
END_PROGRAM

