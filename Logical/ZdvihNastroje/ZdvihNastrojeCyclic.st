(********************************************************************
 * COPYRIGHT -- HP
 ********************************************************************
 * Program: ZdvihNastroje
 * File: ZdvihNastrojeCyclic.st
 * Author: Michal Zvol�nek
 * Created: 9.6.2015
 ********************************************************************
 * Implementation of program ZdvihNastroje
 ********************************************************************)

PROGRAM _CYCLIC
(* TODO : Add your code here *)
	

	PozadovanaVreteno := PozadovanaVreteno;
	StartNajetiV := StartNajetiV;
	Copy := 0;
	StatusPopupNajetiV;
	StartNajetiV;
	
	
	(* Prepocet poloh pro zastaveni*)
		
	PozadovanaDolu1 := PozadovanaVreteno + 50;  //+550
	PozadovanaNahoru1 := PozadovanaVreteno - 100; //-450
	PozadovanaDolu1Pomalu := PozadovanaVreteno + 1500;
	PozadovanaNahoru1Pomalu := PozadovanaVreteno - 1500;



	
		
	IF (Automaticky1Dolu = 1 AND Poloha1 >= PozadovanaDolu1Pomalu) OR (Automaticky1Nahoru = 1 AND Poloha1 <= PozadovanaNahoru1Pomalu) THEN
		Zdvih1Rychle := 1;
	ELSE
		Zdvih1Rychle := 0;
	END_IF
	
	
	
	
	(* Automaticke najeti Dolu*)
		
		
	IF BezpecnostOK = 1 AND StrojZAP = 1 AND StartNajetiV = 1 AND MoveBUTTON = 1 AND Poloha1 > PozadovanaDolu1 THEN
		Automaticky1Dolu := 1;
	ELSIF Automaticky1Dolu = 1 AND Poloha1 <= PozadovanaDolu1 THEN	
		Automaticky1Dolu := 0;
		StartNajetiV := 0;	
	ELSIF BezpecnostOK = 0 OR StrojZAP = 0 OR Sipka1Dolu = 1 OR Sipka1Nahoru = 1 OR Sipka2Dolu = 1 OR Sipka2Nahoru = 1 OR MoveBUTTON = 0 OR GFnaklon = 1 OR KoncakVretenoDolu = 1 THEN
		Automaticky1Dolu := 0;
	END_IF


	(* Automaticke najeti Nahoru*)
		
	IF BezpecnostOK = 1 AND StrojZAP = 1 AND StartNajetiV = 1 AND MoveBUTTON = 1 AND Poloha1 < PozadovanaNahoru1 THEN
		Automaticky1Nahoru := 1;
	ELSIF Automaticky1Nahoru = 1 AND Poloha1 >= PozadovanaNahoru1 THEN	
		Automaticky1Nahoru := 0;
		StartNajetiV := 0;	
	ELSIF BezpecnostOK = 0 OR StrojZAP = 0 OR Sipka1Dolu = 1 OR Sipka1Nahoru = 1 OR Sipka2Dolu = 1 OR Sipka2Nahoru = 1 OR MoveBUTTON = 0 OR GFnaklon = 1 OR KoncakVretenoNahoru = 1 THEN
		Automaticky1Nahoru := 0;
	END_IF
	
	IF BezpecnostOK = 1 AND StrojZAP = 1 THEN
		ZdvihVisuStatus := 2;
		IF (Automaticky1Dolu = 1 OR Sipka1Dolu = 1) AND Automaticky1Nahoru = 0 AND Sipka1Nahoru = 0 AND KoncakVretenoDolu = 0 THEN
			VretenoDolu := 1;
			ZdvihVisuStatus := 1;
			VretenoNahoru := 0;
		ELSE
			VretenoDolu := 0;
		END_IF
		
		IF (Automaticky1Nahoru = 1 OR Sipka1Nahoru = 1) AND Automaticky1Dolu = 0 AND Sipka1Dolu = 0 AND KoncakVretenoNahoru = 0 THEN
			VretenoNahoru := 1;
			ZdvihVisuStatus := 1;
			VretenoDolu := 0;
		ELSE
			VretenoNahoru := 0;
		END_IF
	END_IF	

	IF Zdvih1Rychle = 1 OR Zdvih2Rychle = 1 THEN (* zmena rychlosti najeti prvniho agregatu*)
		VretenoPomalu := 0;	
		VretenoRychle := 1;
	ELSE
		VretenoRychle := 0;
		VretenoPomalu := 1;	
	END_IF
	
	IF ((Automaticky1Nahoru = 1 OR Automaticky1Dolu = 1) OR (Sipka1Dolu = 1 OR Sipka1Nahoru = 1)) THEN	
		GFzdvih := 1;
	ELSE
		GFzdvih := 0;
	END_IF
	
	IF BezpecnostOK = 0 OR StrojZAP = 0 THEN
		ZdvihVisuStatus := 0;
	END_IF
	
	(* reset startunajeti*)
	IF BezpecnostOK = 0 OR StrojZAP = 0 OR Sipka1Dolu = 1 OR Sipka1Nahoru = 1 OR Sipka2Dolu = 1 OR Sipka2Nahoru = 1 OR GFnaklon = 1 OR KoncakVretenoDolu = 1 OR KoncakVretenoNahoru = 1 THEN		
		StartNajetiV := 0;	
	END_IF
	
END_PROGRAM