(********************************************************************
 * COPYRIGHT -- HP
 ********************************************************************
 * Program: HlavniMotor01
 * File: HlavniMotor01Cyclic.st
 * Author: Michal Vavruska
 * Created: February 08, 2012
 ********************************************************************
 * Implementation of program HlavniMotor01
 ********************************************************************)

PROGRAM _CYCLIC
	
	gSetSpeed;
	
	IF BezpecnostOK = 0 OR StrojZAP = 0 OR Stop = 1 THEN
		VretenoOn := 0;
		StatusMotor1 := 0;
	ELSE
		IF VretenoOn = 1 THEN
			StatusMotor1 := 3;
		ELSE
			StatusMotor1 := 1;
			IF TON1reverzOK = 0 THEN
				VretenoOn := 0;
			END_IF
		END_IF
	END_IF
	
	IF (EDGEPOS( Reverz1ZAP)) OR (EDGENEG(Reverz1ZAP)) THEN
		TON1timerENABLED := 1;
	END_IF
	
	TON_Reverz1(IN := TON1timerENABLED , PT :=T#15s );
	
	IF TON1timerENABLED = 1 THEN
		TON1reverzOK := 0;
	END_IF
	
	IF TON_Reverz1.Q THEN
		TON1reverzOK := 1;
		TON1timerENABLED := 0;
	END_IF
	
	IF Reverz1visuZAP = 1 THEN
		Reverz1ZAP := 1;
		Reverz1visuSTAT := 1;
	ELSE 
		Reverz1ZAP := 0;
		Reverz1visuSTAT := 0;
	END_IF
	
	(***************Pou�t�n� vretena*********)
	
	IF BezpecnostOK = 0 OR StrojZAP = 0 THEN
		VretenoOn := 0;
	END_IF
	
	IF NOT gAxis01_ModuleOk THEN
		Step := STEP_INIT;
	ELSIF BasicControl.Status.ErrorID <> 0 OR BasicControl.AxisState.ErrorStop THEN
		Step := STEP_ERROR;
	END_IF;

	CASE Step OF
		STEP_INIT:
			BasicControl.Command.ErrorAcknowledge := 0;
			BasicControl.Command.Power := 0;
			BasicControl.Command.MoveVelocity := 0;
			BasicControl.Command.Home := 0;
			BasicControl.Command.Halt := 0;
			
			IF BasicControl.AxisState.Disabled AND gAxis01_ModuleOk THEN
				Step := STEP_POWER;
			END_IF;	
		
		STEP_POWER:
		
			BasicControl.Command.Power := TRUE;
			Step := STEP_POWER_W;
		
		STEP_POWER_W:
		
			IF BasicControl.Status.DriveStatus.ControllerStatus = TRUE THEN
				
				Step := STEP_HOME;
	 
			END_IF;
		
		STEP_HOME:
		
			BasicControl.Command.Home := TRUE;
			Step := STEP_HOME_W;
		
		STEP_HOME_W:
		
			IF BasicControl.Status.DriveStatus.HomingOk THEN
				BasicControl.Command.Home := 0;
				Step := STEP_READY;
			END_IF;
		
		STEP_READY:
			
			IF OtackyVretena30 THEN
				BasicControl.Parameter.Velocity := SLOW_SPEED*UNITS_PER_REV/60.0;
				BasicControl.Command.MoveVelocity := TRUE;
				Step := STEP_ANGLE_CHANGE;
			ELSIF Otaceni = 1 THEN
				BasicControl.Parameter.Velocity := UINT_TO_REAL(gSetSpeed)*UDINT_TO_REAL(UNITS_PER_REV)/60.0;
				BasicControl.Parameter.Direction := mcPOSITIVE_DIR;
			ELSIF Otaceni = 0 THEN
				BasicControl.Parameter.Velocity := UINT_TO_REAL(gSetSpeed)*UDINT_TO_REAL(UNITS_PER_REV)/60.0;
				BasicControl.Parameter.Direction := mcNEGATIVE_DIR;
			ELSE
				BasicControl.Parameter.Velocity := 0;
			END_IF;
			
			
			IF  Tl_StartVreteno = 1 AND BezpecnostOK = 1 AND StrojZAP = 1 THEN
				BasicControl.Command.MoveVelocity := TRUE;
				Step := STEP_RUN;
			END_IF;
		
		
		
		STEP_RUN:
		
			IF NOT (Tl_StartVreteno = 1 AND BezpecnostOK = 1 AND StrojZAP = 1) THEN
				BasicControl.Command.Halt := TRUE;
				Step := STEP_READY;
			END_IF;
		
		STEP_ANGLE_CHANGE:
		
			IF NOT OtackyVretena30 THEN
				BasicControl.Command.Halt := TRUE;
				Step := STEP_READY;
			END_IF;
		
		
		STEP_ERROR:

			BasicControl.Command.Power := 0;
			BasicControl.Command.MoveVelocity := 0;
			BasicControl.Command.Home := 0;
			BasicControl.Command.Halt := 0;
		
			IF oldError <> BasicControl.Status.ErrorID THEN
				BasicControl.Command.ErrorAcknowledge := 0;
			ELSE
				BasicControl.Command.ErrorAcknowledge := 1;
			END_IF;
		
			IF BasicControl.Status.ErrorID = 0 THEN
				BasicControl.Command.ErrorAcknowledge := 0;
				Step := STEP_POWER;
			END_IF;
		

	END_CASE;
	
	oldError := BasicControl.Status.ErrorID;
	   
	
	(***************volba sm�ru ot��en� v�etena*********)
	
	NeOtaceni := Tl_NeOtaceni;
	SoOtaceni := Tl_SoOtaceni;
	


	(***************pu�t�n� motoru p�i polohov�n�*********)
	
	
	IF OtackyVretena30 = 1 AND Tl_StartVreteno = 0 AND BezpecnostOK = 1 AND StrojZAP = 1 THEN
		OtackyNataveny30 := 1;
	ELSE
		OtackyNataveny30 := 0;
	END_IF	

	
	VretenoOn := Tl_StartVreteno;

	IF  (Tl_StartVreteno = 1 AND BezpecnostOK = 1 AND StrojZAP = 1 ) OR OtackyVretena30 = 1 THEN
		Brzda := 1;
	ELSE
		Brzda := 0;
	END_IF	

		
	
	IF 	TON_BrzdaMotoru.Q THEN
		BrzdaMotoru := 0;
	ELSE
		BrzdaMotoru := 1;
	END_IF	
	
	
	TON_BrzdaMotoru(IN :=(Brzda = 0) , PT := T#3s);
	


END_PROGRAM
