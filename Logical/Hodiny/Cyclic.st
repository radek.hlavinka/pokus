
PROGRAM _CYCLIC
	(* Insert code here *)
	 

	
	IF VretenoZap = 1 THEN 
		
		TON_servisni_sekunda(IN := VretenoServiceTimeCounter, PT := T#1s);
		TON_pracovni_sekunda(IN := VretenoWorkTimeCounter, PT := T#1s);
		
		VretenoServiceTimeCounter := 1;
		VretenoWorkTimeCounter := 1;
			
		
		IF TON_servisni_sekunda.Q THEN
			Servisnisekundy := Servisnisekundy + 1;
			VretenoServiceTimeCounter := 0;
		END_IF
		
		IF Servisnisekundy = 60 THEN
			Servisnisekundy := 0;
			Servisniminuty := Servisniminuty + 1;
		END_IF
		
		IF Servisniminuty = 60 THEN
			Servisniminuty := 0;
			Servisnihodiny := Servisnihodiny + 1;
		END_IF
		
		IF Servisnihodiny = 24 THEN
			Servisnihodiny := 0;
			Servisnidni := Servisnidni + 1;
		END_IF
	
		
		
		IF TON_pracovni_sekunda.Q THEN
			Pracovnisekundy := Pracovnisekundy + 1;
			VretenoWorkTimeCounter := 0;
		END_IF
		
		IF Pracovnisekundy = 60 THEN
			Pracovnisekundy := 0;
			Pracovniminuty := Pracovniminuty + 1;
		END_IF
		
		IF Pracovniminuty = 60 THEN
			Pracovniminuty := 0;
			Pracovnihodiny := Pracovnihodiny + 1;
		END_IF
		
		IF Pracovnihodiny = 24 THEN
			Pracovnihodiny := 0;
			Pracovnidni := Pracovnidni + 1;
		END_IF
		 
		IF ResetPracovnihoCasu = 1 THEN
			Pracovnisekundy := 0;
			Pracovniminuty := 0;	
			Pracovnihodiny := 0;
			Pracovnidni := 0;
			ResetPracovnihoCasu := 0;
		END_IF
	END_IF
END_PROGRAM