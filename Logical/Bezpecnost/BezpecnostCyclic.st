(********************************************************************
 * COPYRIGHT -- HP
 ********************************************************************
 * Program: Bezpecnost
 * File: BezpecnostCyclic.st
 * Author: Michal Zvol�nek
 * Created: 11.6.2015
 ********************************************************************
 * Implementation of program Bezpecnost
 ********************************************************************)

PROGRAM _CYCLIC


(* TODO : Add your code here *)
		
	(* total stop ovladaci panel*)
	IF TotalStopPanel = 0 THEN
		AlarmoveHlaseni[1] := 1; 
	ELSE
		AlarmoveHlaseni[1] := 0;
	END_IF
	TotalStopCepovani := 1;	
//	(* total stop cepovani *)
//	IF TotalStopPanel = 1 AND TotalStopCepovani = 0 THEN
//		AlarmoveHlaseni[2] := 1; 
//	ELSE
		AlarmoveHlaseni[2] := 0;
//	END_IF
			
	(*  otev�ene dvere   *)
	IF TotalStopPanel = 1 AND TotalStopCepovani = 1 AND DvereStroj = 0 THEN
		AlarmoveHlaseni[3] := 1; 
	ELSE
		AlarmoveHlaseni[3] := 0;
	END_IF
	
	(* porucha motoru    *)
	IF TotalStopPanel = 1 AND TotalStopCepovani = 1 AND DvereStroj = 1 AND PoruchaMotoru = 0 THEN
		AlarmoveHlaseni[4] := 1;
	ELSE
		AlarmoveHlaseni[4] := 0;
	END_IF
	
	(*  rezim stroje   *)
	IF TotalStopPanel = 1 AND TotalStopCepovani = 1 AND DvereStroj = 1 AND PoruchaMotoru = 1 AND RezimStrojeProvoz = 0 THEN
		AlarmoveHlaseni[5] := 1;
	ELSE
		AlarmoveHlaseni[5] := 0;
	END_IF
	
	(*********************************************************************************
	*******************Servisni re�im zapnuty ***************************************
	**********************************************************************************)
	
	IF ServisniRezimIn = 1 AND BrzdaMotoru = 0 THEN
		AlarmoveHlaseni[6] := 1;
	ELSE
		AlarmoveHlaseni[6] := 0;
	END_IF
	
	(*********************************************************************************
	*******************Vysunut� �epov�n� ***********************
	**********************************************************************************)
	
	IF CepovaniVysunute = 1 THEN
		AlarmoveHlaseni[8] := 1;
	ELSE
		AlarmoveHlaseni[8] := 0;
	END_IF	
	
	IF TotalStopPanel = 1 AND TotalStopCepovani = 1 AND DvereStroj = 1 AND PoruchaMotoru = 1 AND RezimStrojeProvoz = 1 THEN
		BezpecnostOK := 1; 
	ELSE
		BezpecnostOK := 0;
	END_IF
	
	(*********************************************************************************
	*******************Zm�na ot��ek v�etena ***************************************
	**********************************************************************************)
	
	
	IF ServisniRezimIn = 1 AND EDGEPOS(PotvrzeniANO = 1) THEN
		ZmenaOtacekOK := 1;
	ELSE
		ZmenaOtacekOK := 0;
	END_IF	
	
	IF EDGEPOS (ZmenaOtacekOK = 1) THEN
		Page := 1030;
	END_IF	
	
	IF EDGEPOS(SafetyOut) THEN 
		Page := 10;   (*hlavn� str�nka*)
	END_IF 
END_PROGRAM